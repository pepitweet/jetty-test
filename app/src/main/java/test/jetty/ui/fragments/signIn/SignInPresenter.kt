package test.jetty.ui.fragments.signIn

class SignInPresenter(private val view: SignIn.View) : SignIn.Presenter {
    private val model: SignIn.Model = SignInModel(this)

    override fun performSignIn(email: String, pass: String) = model.performSignIn(email, pass)

    override fun showErrorMessage(msg: Int) = view.showErrorMessage(msg)

    override fun showErrorMessage(msg: Int, values: Pair<Int, String>) = view.showErrorMessage(msg, values)

    override fun showLoading(msg: Int) = view.showLoading(msg)

    override fun hideLoading() = view.hideLoading()

    override fun navigateList() = view.navigateList()
}