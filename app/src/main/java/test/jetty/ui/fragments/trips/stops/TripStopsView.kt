package test.jetty.ui.fragments.trips.stops

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.fragment_trip_stops.*
import kotlinx.android.synthetic.main.layout_vehicle_details.*
import test.jetty.R
import test.jetty.adapters.StopsAdapter
import test.jetty.commons.Const.Params.TRIP_DATA
import test.jetty.commons.ParseContent
import test.jetty.models.Trip
import test.jetty.ui.fragments.BaseFragment

class TripStopsView : BaseFragment() {
    var currentTrip: Trip? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trip_stops, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentTrip = arguments?.getSerializable(TRIP_DATA) as Trip
        fillDetails()
    }

    private fun fillDetails() {
        if (currentTrip != null) {
            val vehicle = currentTrip!!.vehicle
            val driver = currentTrip!!.driver
            tvVehicleDet?.text = ParseContent.getStyledText(
                activity,
                R.string.vehicle_det,
                vehicle.brand,
                vehicle.model)
            tvVehiclePlates?.text = ParseContent.getStyledText(
                activity,
                R.string.vehicle_plate_year,
                vehicle.plate,
                vehicle.year)
            tvDriverDet?.text = ParseContent.getStyledText(
                activity,
                R.string.driver_det,
                driver.name!!,
                driver.lastName!!,
                driver.mobile!!)

            if (vehicle.photo.isNotBlank()) {
                Glide
                    .with(activity)
                    .load(vehicle.photo)
                    .fallback(R.drawable.ic_jetty_logo)
                    .placeholder(R.drawable.ic_jetty_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerInside()
                    .into(ivVehicle)
            }

            slTripStops.setAdapter(StopsAdapter(currentTrip!!.tripStops))
        } else {
            Toast.makeText(activity, getString(R.string.no_trip_data), Toast.LENGTH_SHORT).show()
            findNavController().popBackStack()
        }
    }
}