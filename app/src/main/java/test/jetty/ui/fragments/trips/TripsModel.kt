package test.jetty.ui.fragments.trips

import android.content.Context
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import test.jetty.R
import test.jetty.adapters.TripsAdapter
import test.jetty.api.RetrofitHelper
import test.jetty.commons.ParseContent
import test.jetty.models.Trip
import test.jetty.session.SessionData
import java.util.*

class TripsModel(private val presenter: Trips.Presenter) : Trips.Model {

    override fun updateTrips(context: Context) {
        GlobalScope.launch {
            presenter.showLoading(R.string.loading)
            val result = RetrofitHelper.getTrips(SessionData.currentUser?.email!!)
            if (result.statusCode == 200) {
                val data = getTripsAndSeparatorIdx(result.objectT!!)
                presenter.updateRecyclerData(TripsAdapter(data.first, data.second))
            } else if (result.statusCode == 401)
                presenter.logout(true)
            else {
                presenter.showErrorMessage(R.string.error_trip_list)
            }
            presenter.hideLoading()
        }
    }

    private fun getTripsAndSeparatorIdx(tripList: ArrayList<Trip>) : Pair<ArrayList<Trip>, TreeSet<Int>> {
        val dateIndexes = TreeSet<Int>()
        var lastDate = ""

        Collections.sort(tripList, Comparator { o1, o2 ->
            return@Comparator o1.dateToSort!!.compareTo(o2.dateToSort)
        })

        for ((index, currentTrip) in tripList.withIndex()) {
            val currentTripDate = ParseContent.Format.formatPrettyDate.format(currentTrip.date!!)
            if (lastDate.compareTo(currentTripDate) != 0) {
                lastDate = currentTripDate
                dateIndexes.add(index)
            }
        }

        return Pair(tripList, dateIndexes)
    }

}