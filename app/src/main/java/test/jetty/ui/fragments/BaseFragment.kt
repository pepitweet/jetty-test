package test.jetty.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import test.jetty.R
import test.jetty.ui.activities.MainActivity



abstract class BaseFragment : Fragment() {
    lateinit var activity: MainActivity


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as MainActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        hideKeyboard()
    }

    private fun hideToolbar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
    }

    fun showToolbar(title: String? = null) {
        (activity as AppCompatActivity).supportActionBar!!.show()
        title?.let {
            (activity as AppCompatActivity).title = title
        }
    }

    fun hideKeyboard() {
        val imm: InputMethodManager? = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val currentFocusedView = activity.currentFocus

        if (imm != null && currentFocusedView != null)
            imm.hideSoftInputFromWindow(currentFocusedView.windowToken, 0)
    }

    fun goToLogin(tkExp: Boolean = false) {
        if (tkExp) {
            activity.runOnUiThread {
                Toast.makeText(activity, R.string.tk_expired, Toast.LENGTH_LONG).show()
            }
        }

        findNavController().navigate(
            R.id.signInView,
            null,
            NavOptions.Builder().setPopUpTo(
                R.id.signInView,
                true)
                .build())
    }

}