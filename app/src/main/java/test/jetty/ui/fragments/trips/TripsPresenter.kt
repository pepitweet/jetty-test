package test.jetty.ui.fragments.trips

import android.content.Context
import test.jetty.adapters.TripsAdapter

class TripsPresenter(val view: Trips.View) : Trips.Presenter {
    private val model: Trips.Model = TripsModel(this)

    override fun logout(tkExp: Boolean) = view.logout(tkExp)
    
    override fun showLoading(msg: Int)  = view.showLoading(msg)

    override fun hideLoading() = view.hideLoading()

    override fun showErrorMessage(msg: Int) = view.showErrorMessage(msg)

    override fun updateRecyclerData(adapter: TripsAdapter) = view.updateRecyclerData(adapter)

    override fun updateTrips(context: Context) = model.updateTrips(context)
}