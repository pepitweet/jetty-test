package test.jetty.ui.fragments.signIn

interface SignIn {
    interface View {
        fun showErrorMessage(msg: Int)
        fun showErrorMessage(msg: Int, values: Pair<Int, String>)
        fun showLoading(msg: Int)
        fun hideLoading()
        fun navigateList()
    }

    interface Presenter {
        fun navigateList()
        fun showErrorMessage(msg: Int)
        fun showErrorMessage(msg: Int, values: Pair<Int, String>)
        fun showLoading(msg: Int)
        fun hideLoading()

        fun performSignIn(email: String, pass: String)
    }

    interface Model {
        fun performSignIn(email: String, pass: String)
    }
}