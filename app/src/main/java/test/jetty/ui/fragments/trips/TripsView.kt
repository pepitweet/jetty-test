package test.jetty.ui.fragments.trips

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.labters.lottiealertdialoglibrary.ClickListener
import com.labters.lottiealertdialoglibrary.DialogTypes
import com.labters.lottiealertdialoglibrary.LottieAlertDialog
import kotlinx.android.synthetic.main.fragment_trips.*
import test.jetty.R
import test.jetty.adapters.TripsAdapter
import test.jetty.commons.Const.Params.TRIP_DATA
import test.jetty.models.Trip
import test.jetty.session.SessionData
import test.jetty.ui.activities.MainActivity
import test.jetty.ui.fragments.BaseFragment

class TripsView : BaseFragment(), Trips.View {
    lateinit var presenter: Trips.Presenter
    var alertDialog: LottieAlertDialog? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as MainActivity
        requireActivity().onBackPressedDispatcher.addCallback(
            owner = this,
            onBackPressed = {
                performBackPressed()
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_trips,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = TripsPresenter(this)
        showToolbar(getString(R.string.tripsAssigned))
        presenter.updateTrips(activity)
    }

    override fun showLoading(msg: Int) {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()

        activity.runOnUiThread {
            alertDialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_LOADING)
                .setDescription(getString(msg))
                .build()
            alertDialog?.setCancelable(false)
            alertDialog?.show()
        }
    }

    override fun hideLoading() {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()
    }

    override fun updateRecyclerData(adapter: TripsAdapter) {
        activity.runOnUiThread {
            rvTrips?.adapter = adapter
            rvTrips?.layoutManager = LinearLayoutManager(activity)
            adapter.setOnSelectListener {
                showDetails(it)
            }
        }
    }

    override fun showErrorMessage(msg: Int) {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()

        activity.runOnUiThread {
            alertDialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_ERROR)
                .setDescription(getString(msg))
                .setPositiveText(getString(R.string.ok))
                .setPositiveButtonColor(activity.resources.getColor(R.color.colorPrimary))
                .setPositiveTextColor(activity.resources.getColor(R.color.colorTextWhite))
                .setPositiveListener(object : ClickListener {
                    override fun onClick(dialog: LottieAlertDialog) {
                        dialog.dismiss()
                    }
                })
                .build()
            alertDialog?.setCancelable(false)
            alertDialog?.show()
        }
    }

    override fun logout(tkExp: Boolean) {
        if (tkExp) SessionData.currentUser = null
        goToLogin(tkExp)
    }

    private fun showDetails(trip: Trip) {
        val bundle = bundleOf(TRIP_DATA to trip)
        findNavController().navigate(R.id.action_tripsView_to_tripStopsView, bundle)
    }

    private fun performBackPressed() {
        val dialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_QUESTION)
            .setDescription(getString(R.string.logout))
            .setPositiveText(getString(R.string.ok))
            .setPositiveButtonColor(resources.getColor(R.color.colorPrimary))
            .setPositiveTextColor(resources.getColor(R.color.colorTextWhite))
            .setPositiveListener(object : ClickListener {
                override fun onClick(dialog: LottieAlertDialog) {
                    dialog.dismiss()
                    findNavController().popBackStack()
                }
            })
            .setNegativeText("No")
            .setPositiveButtonColor(resources.getColor(R.color.colorPrimary))
            .setPositiveTextColor(resources.getColor(R.color.colorTextWhite))
            .setNegativeListener(
                object : ClickListener {
                    override fun onClick(dialog: LottieAlertDialog) {
                        dialog.dismiss()
                    }
                }
            )
            .build()
        dialog.setCancelable(false)
        dialog.show()
    }
}