package test.jetty.ui.fragments.signIn

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.labters.lottiealertdialoglibrary.ClickListener
import com.labters.lottiealertdialoglibrary.DialogTypes
import com.labters.lottiealertdialoglibrary.LottieAlertDialog
import kotlinx.android.synthetic.main.fragment_sign_in.*
import test.jetty.R
import test.jetty.ui.fragments.BaseFragment

class SignInView : BaseFragment(), SignIn.View {
    lateinit var presenter: SignIn.Presenter
    var alertDialog: LottieAlertDialog? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignIn.setOnClickListener {
            hideKeyboard()
            presenter.performSignIn(tiedCorreo.text.toString(), tiedPassword.text.toString())
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = SignInPresenter(this)
    }

    override fun showErrorMessage(msg: Int) {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()

        activity.runOnUiThread {
            alertDialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_ERROR)
                .setDescription(getString(msg))
                .setPositiveText(getString(R.string.ok))
                .setPositiveButtonColor(activity.resources.getColor(R.color.colorPrimary))
                .setPositiveTextColor(activity.resources.getColor(R.color.colorTextWhite))
                .setPositiveListener(object : ClickListener {
                    override fun onClick(dialog: LottieAlertDialog) {
                        dialog.dismiss()
                    }
                })
                .build()
            alertDialog?.setCancelable(false)
            alertDialog?.show()
        }
    }

    override fun showErrorMessage(msg: Int, values: Pair<Int, String>) {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()

        activity.runOnUiThread {
            alertDialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_ERROR)
                .setDescription(getString(msg, values.second, values.first))
                .setPositiveText(getString(R.string.ok))
                .setPositiveButtonColor(activity.resources.getColor(R.color.colorPrimary))
                .setPositiveTextColor(activity.resources.getColor(R.color.colorTextWhite))
                .setPositiveListener(object : ClickListener {
                    override fun onClick(dialog: LottieAlertDialog) {
                        dialog.dismiss()
                    }
                })
                .build()
            alertDialog?.setCancelable(false)
            alertDialog?.show()
        }
    }

    override fun showLoading(msg: Int) {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()

        activity.runOnUiThread {
            alertDialog = LottieAlertDialog.Builder(activity, DialogTypes.TYPE_LOADING)
                .setDescription(getString(msg))
                .build()
            alertDialog?.setCancelable(false)
            alertDialog?.show()
        }
    }

    override fun hideLoading() {
        if (alertDialog != null && alertDialog!!.isShowing) alertDialog?.dismiss()
    }

    override fun navigateList() {
        activity.runOnUiThread {
            tiedCorreo.text?.clear()
            tiedPassword.text?.clear()
        }
        findNavController().navigate(R.id.action_signInView_to_tripsView)
    }
}