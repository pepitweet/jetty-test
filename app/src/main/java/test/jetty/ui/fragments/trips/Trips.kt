package test.jetty.ui.fragments.trips

import android.content.Context
import test.jetty.adapters.TripsAdapter

interface Trips {
    interface View {
        fun updateRecyclerData(adapter: TripsAdapter)
        fun showLoading(msg: Int)
        fun hideLoading()
        fun showErrorMessage(msg: Int)
        fun logout(tkExp: Boolean = false)
    }

    interface Presenter {
        fun updateRecyclerData(adapter: TripsAdapter)
        fun showLoading(msg: Int)
        fun hideLoading()
        fun showErrorMessage(msg: Int)
        fun logout(tkExp: Boolean = false)

        fun updateTrips(context: Context)
    }

    interface Model {
        fun updateTrips(context: Context)
    }
}