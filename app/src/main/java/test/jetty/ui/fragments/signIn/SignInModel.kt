package test.jetty.ui.fragments.signIn

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import test.jetty.R
import test.jetty.api.RetrofitHelper
import test.jetty.commons.ParseContent
import test.jetty.session.SessionData

class SignInModel(private val presenter: SignIn.Presenter) :  SignIn.Model {

    override fun performSignIn(email: String, pass: String) {
        if (ParseContent.isValidEmail(email)) {
            GlobalScope.launch {
                presenter.showLoading(R.string.starting)
                val result = RetrofitHelper.signIn(email, pass)
                presenter.hideLoading()
                when {
                    result.throwable != null -> presenter.showErrorMessage(
                        R.string.sign_in_error,
                        Pair(0, result.throwable?.message!!))
                    result.statusCode == 200 -> {
                        SessionData.currentUser = result.objectT
                        presenter.navigateList()
                    }
                    result.errorObject != null -> presenter.showErrorMessage(
                        R.string.sign_error_body,
                        Pair(result.errorObject?.code!!, result.errorObject?.message!!))
                    else -> presenter.showErrorMessage(
                        R.string.sign_in_error,
                        Pair(0, ""))
                }
            }
        } else {
            presenter.showErrorMessage(R.string.invalid_email)
        }
    }


}