package test.jetty.commons

import android.content.Context
import android.text.Html
import android.util.Patterns
import test.jetty.commons.Const.DEPRECATION
import test.jetty.session.SessionData
import java.text.SimpleDateFormat
import java.util.*

object ParseContent {

    fun isValidEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }

    @Suppress(DEPRECATION)
    fun getStyledText(context: Context, resourceId: Int, vararg args: Any): CharSequence {
        val text = String.format(context.getString(resourceId), *args)

        return Html.fromHtml(text)
    }

    object Format {
        private const val DATE = "yyyy-MM-dd"
        private const val DATE_PRETTY = "dd MMMM"
        private const val DATE_TIME = "$DATE h:mm a"
        private const val DEFAULT_LOCALE = "es-MX"

        val formatDefaultDate: SimpleDateFormat by lazy {
            SimpleDateFormat(DATE, SessionData.locale)
        }

        val formatPrettyDate: SimpleDateFormat by lazy {
            SimpleDateFormat(DATE_PRETTY, SessionData.locale)
        }

        val formatDateTime: SimpleDateFormat by lazy {
            SimpleDateFormat(DATE_TIME, Locale(DEFAULT_LOCALE))
        }
    }
}