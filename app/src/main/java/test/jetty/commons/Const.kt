package test.jetty.commons

object Const {
    const val BASE_URL = "https://jettymx-st.herokuapp.com/api/"

    const val DEF_NO_TIME = "N/D"
    const val DEPRECATION = "DEPRECATION"

    object EndPoints {
        const val SIGN_IN = "drivers/session"
        const val GET_TRIPS = "drivers/trips"
    }

    object Params {
        const val DRIVER = "driver"
        const val ID = "id"
        const val NAME = "name"
        const val LAST_NAME = "last_name"
        const val MOBILE = "mobile"
        const val PHOTO = "photo"
        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val CODE = "code"
        const val MESSAGE = "message"
        const val AUTH_TOKEN = "auth_token"

        const val BRAND = "brand"
        const val MODEL = "model"
        const val OWNER = "owner"
        const val PLATE = "plate"
        const val SEATS_NUMBER = "seats_number"
        const val YEAR = "year"

        const val ADDRESS = "address"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val DEPARTURE_TIME = "departure_time"
        const val ARRIVAL_TIME = "arrival_time"
        const val BOARDING = "boarding"

        const val PRICE = "price"
        const val DATE = "date"
        const val TIME = "time"
        const val VEHICLE = "vehicle"
        const val STATUS = "status"
        const val IS_PRIVATE = "is_private"
        const val TRIP_STOPS = "trip_stops"

        const val HEADER_AUT = "Token %s,email=%s"
        const val AUTHORIZATION = "Authorization"

        const val TRIP_DATA = "TRIP_DATA"
    }
}