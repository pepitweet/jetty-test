package test.jetty.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_item_trip.view.*
import test.jetty.R
import test.jetty.models.Trip
import java.util.*

class TripsAdapter(
    private val trips: ArrayList<Trip>,
    private val separators: TreeSet<Int>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var selectListener: (Trip) -> Unit = { }

    fun setOnSelectListener(listener: (Trip) -> Unit) {
        selectListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.custom_item_trip, parent, false)
        return TripsViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val trip = trips[position]
        val holder = viewHolder as TripsViewHolder

        if (separators.contains(position)) {
            holder.llSeparator?.visibility = View.VISIBLE
            holder.tvDate?.text = trip.dateHeaderStr
        } else
            holder.llSeparator?.visibility = View.GONE

        holder.tvRouteName?.text = trip.name
        holder.tvDepartureTime?.text = trip.time
        holder.tvFirstStop?.text = trip.firstStop?.address
        holder.tvLastStop?.text = trip.lastStop?.address
        holder.btnViewStops?.setOnClickListener {
            selectListener(trip)
        }
    }

    override fun getItemCount() = trips.size

    class TripsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llSeparator: LinearLayoutCompat? = itemView.llSeparator
        val tvDate: AppCompatTextView? = itemView.tvDate
        val tvRouteName: AppCompatTextView? = itemView.tvRouteName
        val tvDepartureTime: AppCompatTextView? = itemView.tvDepartureTime
        val tvFirstStop: AppCompatTextView? = itemView.tvFirstStop
        val tvLastStop: AppCompatTextView? = itemView.tvLastStop
        val btnViewStops: AppCompatButton? = itemView.btnViewStops
    }
}