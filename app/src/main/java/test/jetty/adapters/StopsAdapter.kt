package test.jetty.adapters

import com.transferwise.sequencelayout.SequenceAdapter
import com.transferwise.sequencelayout.SequenceStep
import test.jetty.commons.Const.DEF_NO_TIME
import test.jetty.models.Stops

class StopsAdapter(private val items: List<Stops>) : SequenceAdapter<Stops>() {

    override fun bindView(sequenceStep: SequenceStep, item: Stops) {
        with(sequenceStep) {
            setActive(false)
            setAnchor(
                if (item.departureTime != null)
                    item.departureTime
                else
                    DEF_NO_TIME
            )
            setTitle(item.name)
            setSubtitle(item.address)
        }
    }

    override fun getItem(position: Int) = items[position]

    override fun getCount() = items.size

}