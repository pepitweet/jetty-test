package test.jetty.models

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.ADDRESS
import test.jetty.commons.Const.Params.ARRIVAL_TIME
import test.jetty.commons.Const.Params.BOARDING
import test.jetty.commons.Const.Params.DEPARTURE_TIME
import test.jetty.commons.Const.Params.ID
import test.jetty.commons.Const.Params.LATITUDE
import test.jetty.commons.Const.Params.LONGITUDE
import test.jetty.commons.Const.Params.NAME

data class Stops(
    @SerializedName(ID) var id: Int,
    @SerializedName(NAME) var name: String,
    @SerializedName(ADDRESS) var address: String,
    @SerializedName(LATITUDE) var latitude: String,
    @SerializedName(LONGITUDE) var longitude: String,
    @SerializedName(DEPARTURE_TIME) var departureTime: String?,
    @SerializedName(ARRIVAL_TIME) var arrivalTime: String?,
    @SerializedName(BOARDING) var boarding: Boolean
)