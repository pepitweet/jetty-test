package test.jetty.models

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.DATE
import test.jetty.commons.Const.Params.DRIVER
import test.jetty.commons.Const.Params.ID
import test.jetty.commons.Const.Params.IS_PRIVATE
import test.jetty.commons.Const.Params.NAME
import test.jetty.commons.Const.Params.PRICE
import test.jetty.commons.Const.Params.STATUS
import test.jetty.commons.Const.Params.TIME
import test.jetty.commons.Const.Params.TRIP_STOPS
import test.jetty.commons.Const.Params.VEHICLE
import test.jetty.commons.ParseContent
import java.io.Serializable
import java.util.*

data class Trip(
    @SerializedName(ID) var id: Int,
    @SerializedName(NAME) var name: String,
    @SerializedName(PRICE) var price: String,
    @SerializedName(DATE) var dateStr: String,
    @SerializedName(TIME) var time: String,
    @SerializedName(DRIVER) var driver: Driver,
    @SerializedName(VEHICLE) var vehicle: Vehicle,
    @SerializedName(STATUS) var status: String,
    @SerializedName(IS_PRIVATE) var isPrivate: Boolean,
    @SerializedName(TRIP_STOPS) var tripStops: ArrayList<Stops>
) : Serializable {
    val firstStop: Stops?
        get() = tripStops.first()

    val lastStop: Stops?
        get() = tripStops.last()

    val date: Date?
        get() = ParseContent.Format.formatDefaultDate.parse(dateStr)

    val dateHeaderStr: String?
        get() =
            if (date != null)
                ParseContent.Format.formatPrettyDate.format(date!!)
            else
                null

    val dateToSort: Date?
        get() = ParseContent.Format.formatDateTime.parse("$dateStr $time")
}