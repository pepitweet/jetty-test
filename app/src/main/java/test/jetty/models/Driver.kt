package test.jetty.models

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.AUTH_TOKEN
import test.jetty.commons.Const.Params.EMAIL
import test.jetty.commons.Const.Params.ID
import test.jetty.commons.Const.Params.LAST_NAME
import test.jetty.commons.Const.Params.MOBILE
import test.jetty.commons.Const.Params.NAME
import test.jetty.commons.Const.Params.PASSWORD
import test.jetty.commons.Const.Params.PHOTO

data class Driver(
    @SerializedName(ID) var id: Int? = null,
    @SerializedName(NAME) var name: String? = null,
    @SerializedName(LAST_NAME) var lastName: String? = null,
    @SerializedName(MOBILE) var mobile: String? = null,
    @SerializedName(PHOTO) var photo: String? = null,
    @SerializedName(EMAIL) var email: String? = null,
    @SerializedName(PASSWORD) var password: String? = null,
    @SerializedName(AUTH_TOKEN) var authToken: String? = null)