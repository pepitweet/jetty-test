package test.jetty.models

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.BRAND
import test.jetty.commons.Const.Params.ID
import test.jetty.commons.Const.Params.MODEL
import test.jetty.commons.Const.Params.OWNER
import test.jetty.commons.Const.Params.PHOTO
import test.jetty.commons.Const.Params.PLATE
import test.jetty.commons.Const.Params.SEATS_NUMBER
import test.jetty.commons.Const.Params.YEAR

data class Vehicle(
    @SerializedName(ID) var id: Int,
    @SerializedName(BRAND) var brand: String,
    @SerializedName(MODEL) var model: String,
    @SerializedName(OWNER) var owner: String,
    @SerializedName(PLATE) var plate: String,
    @SerializedName(SEATS_NUMBER) var seatsNumber: Int,
    @SerializedName(YEAR) var year: String,
    @SerializedName(PHOTO) var photo: String
)