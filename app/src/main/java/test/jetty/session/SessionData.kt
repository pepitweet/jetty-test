package test.jetty.session

import test.jetty.models.Driver
import java.util.*

object SessionData {
    var currentUser: Driver? = null
    var locale: Locale = Locale.getDefault()
}