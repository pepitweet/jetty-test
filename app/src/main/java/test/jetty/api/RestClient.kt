package test.jetty.api

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import test.jetty.api.request.SignInRequest
import test.jetty.commons.Const.EndPoints.GET_TRIPS
import test.jetty.commons.Const.EndPoints.SIGN_IN
import test.jetty.commons.Const.Params.AUTHORIZATION
import test.jetty.models.Driver
import test.jetty.models.Trip

interface RestClient {

    @POST(SIGN_IN)
    fun signIn(@Body request: SignInRequest): Call<Driver>

    @GET(GET_TRIPS)
    fun getTrips(@Header(AUTHORIZATION) auth: String): Call<ArrayList<Trip>>
}