package test.jetty.api.request

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.DRIVER
import test.jetty.models.Driver

data class SignInRequest(
    @SerializedName(DRIVER) val driver: Driver? = null
)