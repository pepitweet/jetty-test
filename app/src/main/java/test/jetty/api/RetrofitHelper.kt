package test.jetty.api

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.jetty.api.request.SignInRequest
import test.jetty.api.response.DefaultResponse
import test.jetty.commons.Const.BASE_URL
import test.jetty.commons.Const.Params.HEADER_AUT
import test.jetty.models.Driver
import test.jetty.models.Trip
import test.jetty.session.SessionData
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private const val CONNECTION_TIMEOUT:Long = 10000
private const val DATA_RETRIEVAL_TIMEOUT:Long = 30000

object RetrofitHelper {

    private val client = OkHttpClient.Builder()
        .callTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
        .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
        .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
        .readTimeout(DATA_RETRIEVAL_TIMEOUT, TimeUnit.MILLISECONDS)
        .build()

    private val builder = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(builder))
        .build()

    private val restClient = retrofit.create(RestClient::class.java)

    suspend fun signIn(email: String, password: String): RequestResponse<Driver> {
        val call = restClient.signIn(SignInRequest(Driver(email = email, password = password)))
        return performRequest(call)
    }

    suspend fun getTrips(email: String): RequestResponse<ArrayList<Trip>> {
        val call = restClient.getTrips(String.format(HEADER_AUT, SessionData.currentUser?.authToken, email))
        return performRequest(call)
    }

    private suspend fun <T>performRequest(
        call: Call<T>
    ) = suspendCoroutine<RequestResponse<T>> {  continuation ->
        val result = RequestResponse<T>()

        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                result.statusCode = response.code()
                if (response.isSuccessful)
                    result.objectT = response.body()
                else {
                    try {
                        result.errorObject = Gson().fromJson(
                            response.errorBody()!!.string(),
                            DefaultResponse::class.java
                        )
                    } catch (e: Exception) {
                        result.errorObject = DefaultResponse(
                            900,
                            "Error inesperado")
                    }
                }

                continuation.resume(result)
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                result.throwable = t
                continuation.resume(result)
            }
        })
    }
}