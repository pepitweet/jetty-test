package test.jetty.api.response

import com.google.gson.annotations.SerializedName
import test.jetty.commons.Const.Params.CODE
import test.jetty.commons.Const.Params.MESSAGE

open class DefaultResponse(
    @SerializedName(CODE) val code: Int? = null,
    @SerializedName(MESSAGE) val message: String? = null)