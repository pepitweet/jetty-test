package test.jetty.api

import test.jetty.api.response.DefaultResponse

class RequestResponse<T> {
    var objectT: T? = null
    var statusCode: Int  = 0
    var throwable: Throwable? = null
    var errorObject: DefaultResponse? = null
}