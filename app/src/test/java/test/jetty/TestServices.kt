package test.jetty

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import test.jetty.api.RetrofitHelper
import test.jetty.session.SessionData

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestServices {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun login() {
        runBlocking {
            val response = RetrofitHelper.signIn("irvin@jetty.mx", "123456")
            if (response.statusCode == 401) {
                val result = response.errorObject
                println("code: ${result?.code} msg: ${result?.message}")
                assert(true)
            } else {
                val result = response.objectT
                if (result != null) {
                    SessionData.currentUser = response.objectT
                    println("Nombre: ${result.name} ${result.lastName}")
                } else {
                    if (response.throwable != null)
                        println("${response.throwable!!.message}")
                    else
                        assert(false)
                }
            }
        }
    }

    @Test
    fun getTrips() {
        login()
        runBlocking {
            val response = RetrofitHelper.getTrips(SessionData.currentUser?.email!!)
            if (response.statusCode == 200) {
                for (trip in response.objectT!!) {
                    println("${trip.name} FECHA: ${trip.date}")
                }
            }
        }
    }
}
